package testwordwrapkata;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import wordwrapkata.Wrapper;

class WrapperTest {

	private static final String LINE_BREAK = System.lineSeparator();

	@BeforeEach
	public void initEach() {
		System.out.println("BeforeEach new Wrap method test");
	}

	@Test
	@DisplayName("AssertALL")
	public void testMultiply() {
		assertAll(() -> assertEquals("line", Wrapper.wrap("line", 7)),
				() -> assertEquals("line" + LINE_BREAK + "line", Wrapper.wrap("line line", 6)),
				() -> assertEquals("line" + LINE_BREAK + "line", Wrapper.wrap("line line", 4)),
				() -> assertEquals("line" + LINE_BREAK + "line", Wrapper.wrap("line line", 5)),
				() -> assertEquals("line" + LINE_BREAK + "line", Wrapper.wrap("lineline", 4)),
				() -> assertEquals("line" + LINE_BREAK + "line" + LINE_BREAK + "line", Wrapper.wrap("linelineline", 4)),
				() -> assertEquals("line" + LINE_BREAK + "lome" + LINE_BREAK + "lane" + LINE_BREAK + "lune",
						Wrapper.wrap("linelome lanelune", 4)),
				() -> assertEquals(
						"Today I" + LINE_BREAK + "have an" + LINE_BREAK + "Interview" + LINE_BREAK + "in CGI",
						Wrapper.wrap("Today I have an Interview in CGI", 9)),
				() -> assertEquals(
						"Hi" + LINE_BREAK + "Nigga" + LINE_BREAK + "! How" + LINE_BREAK + "are" + LINE_BREAK + "you"
								+ LINE_BREAK + "today" + LINE_BREAK + "? I" + LINE_BREAK + "am" + LINE_BREAK + "fine.",
						Wrapper.wrap("Hi Nigga! How are you today? I am fine.", 5)),
				() -> assertEquals("HalloM" + LINE_BREAK + "einNam" + LINE_BREAK + "eistKl" + LINE_BREAK + "etus",
						Wrapper.wrap("HalloMeinNameistKletus", 6)),
				() -> assertEquals("Hallo" + LINE_BREAK + "MeinN" + LINE_BREAK + "ameis" + LINE_BREAK + "tKlet"
						+ LINE_BREAK + "us", Wrapper.wrap("HalloMeinNameistKletus", 5)),
				() -> assertEquals(
						"Hallo" + LINE_BREAK + "MeinN" + LINE_BREAK + "ameis" + LINE_BREAK + "tKlet" + LINE_BREAK + "us"
								+ LINE_BREAK + "TheGr" + LINE_BREAK + "eatMa" + LINE_BREAK + "ster",
						Wrapper.wrap("HalloMeinNameistKletus TheGreatMaster", 5)));

	}

}
