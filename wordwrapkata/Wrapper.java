package wordwrapkata;

public class Wrapper {
	
	private Wrapper() {//All members are static
	}

	private static final String LINE_BREAK = System.lineSeparator();

	public static String wrap(final String line, final int numberColumn) {

		final StringBuffer lineToPrint = new StringBuffer();

		return (line.length() < numberColumn) ? line : lineToPrint.append(performLongLines(line, numberColumn, lineToPrint)).toString();
	}

	/**
	 * This method separates the input text line into words of maximum length of
	 * a number of columns provided. It inserts a line break after each word
	 * boundary having in mind if the line has spaces or not.
	 * 
	 * It returns the result line required on the Word-Wrap-Kata.
	 * 
	 * @param line:
	 *            Input text line of the Kata with OR without spaces and with a
	 *            length of more than numberColumn.
	 * 
	 * @param lineToPrint:
	 *            Buffer to concatenate the desired result.
	 *
	 **/

	public static String performLongLines(String line, final int numberColumn, final StringBuffer lineToPrint) {
		while (line.length() > numberColumn) {
			int indexOfSpace = line.lastIndexOf(" ", numberColumn);
			if (indexOfSpace != -1) {
				String word = line.substring(0, indexOfSpace);

				lineToPrint.append(word).append(LINE_BREAK);
				line = line.substring(indexOfSpace + 1);

			} else {
				if (indexOfSpace == -1) {
					line = processlineWithoutSpacesBetween(line, lineToPrint, numberColumn);
					continue;
				}
			}
		}
		return line;
	}

	/**
	 * This method cuts the line extracting the first word with the maximum
	 * length of number of columns specified. After each word, a line break is
	 * inserted and both are concatenated in lineToPrint. The input line is also
	 * updated.
	 * 
	 * It returns an updated line of the input after extracting the first word
	 * with a line break.
	 * 
	 * @param line:
	 *            Input text line of the Kata without spaces and with a length
	 *            of more than numberColumn.
	 * 
	 * @param lineToPrint:
	 *            Buffer to concatenate the desired result.
	 * 
	 * @param numberColumn:
	 *            Max number of columns to break the input line.
	 **/

	private static String processlineWithoutSpacesBetween(final String line, final StringBuffer lineToPrint, final int numberColumn) {
		String lineAux = new String(line);
		String wordWithoutSpaces = line.substring(0, numberColumn);
		lineToPrint.append(wordWithoutSpaces).append(LINE_BREAK);
		lineAux = line.substring(numberColumn);
		return lineAux;
	}
}
